package th.ac.tu.siit.lab7database;

import android.os.Bundle;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends ListActivity {
	
	DBHelper dbHelper;
	SQLiteDatabase db;
	Cursor cursor;
	SimpleCursorAdapter adapter;
	int position;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dbHelper = new DBHelper(this);
		db = dbHelper.getWritableDatabase();
		cursor = getAllContacts();
		adapter = new SimpleCursorAdapter(this, R.layout.item, cursor, 
				new String[] {"ct_name", "ct_phone", "ct_type", "ct_email"},
				new int[] {R.id.tvName, R.id.tvPhone, R.id.ivPhoneType, R.id.tvEmail}, 0);
		setListAdapter(adapter);
		registerForContextMenu(getListView());
	}
	
	private Cursor getAllContacts() {
		return db.query("contacts", 
				new String[] {"_id", "ct_name", "ct_phone", "ct_type", "ct_email"}, 
				null, null, null, null, "ct_name asc");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.context, menu);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		cursor.close();
		db.close();
		dbHelper.close();
		finish();
	}

	@Override
	protected void onActivityResult(int requestCode, 
			int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			ContentValues v = new ContentValues();
			v.put("ct_name", data.getStringExtra("name"));
			v.put("ct_phone", data.getStringExtra("phone"));
			v.put("ct_email", data.getStringExtra("email"));
			v.put("ct_type", data.getStringExtra("type"));
			db.insert("contacts", null, v);
			cursor = getAllContacts();
			adapter.changeCursor(cursor);
			adapter.notifyDataSetChanged();
		}else if(requestCode == 8888 && resultCode == RESULT_OK){
			ContentValues v = new ContentValues();
			v.put("ct_name", data.getStringExtra("name"));
			v.put("ct_phone", data.getStringExtra("phone"));
			v.put("ct_email", data.getStringExtra("email"));
			v.put("ct_type", data.getStringExtra("type"));
			long id = data.getLongExtra("id", 0);
			String selection = "_id = ?";
			String[] selectionArgs = { ""+id };
			db.update("contacts", v, selection, selectionArgs);
			cursor = getAllContacts();
			adapter.changeCursor(cursor);
			adapter.notifyDataSetChanged();
		}else{
			super.onActivityResult(requestCode, resultCode, data);
			
		} 	
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_new:
			Intent i = new Intent(this, AddNewActivity.class);
			startActivityForResult(i, 9999);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo a = (AdapterContextMenuInfo)item.getMenuInfo();
		long id = a.id;
		int position = a.position;
		
		switch(item.getItemId()) {
		case R.id.action_edit:
			Cursor c = (Cursor)adapter.getItem(position);
			String name = c.getString(c.getColumnIndex("ct_name"));
			String phone = c.getString(c.getColumnIndex("ct_phone"));
			String email = c.getString(c.getColumnIndex("ct_email"));
			int ctype = c.getInt(c.getColumnIndex("ct_type"));
			Intent i = new Intent(this, AddNewActivity.class);
			i.putExtra("name", name);
			i.putExtra("phone", phone);
			i.putExtra("email", email);
			i.putExtra("type", ctype);
			i.putExtra("id", id);
			startActivityForResult(i, 8888);
			cursor = getAllContacts();
			adapter.changeCursor(cursor);
			adapter.notifyDataSetChanged();
			return true;
		case R.id.action_delete:
			String selection = "_id = ?";
			String[] selectionArgs = { String.valueOf(id) }; 
			db.delete("contacts", selection, selectionArgs);
			cursor = getAllContacts();
			adapter.changeCursor(cursor);
			adapter.notifyDataSetChanged();
			return true;
		}
		return super.onContextItemSelected(item);
	}
}
